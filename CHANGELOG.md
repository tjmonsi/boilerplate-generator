# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.2 (2020-10-17)

### [0.1.2](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/compare/v0.1.1...v0.1.2) (2020-08-09)


### Bug Fixes

* fix dependencies ([9bd8e34](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/9bd8e3408780f44ae586d625f10378495b34c9ec))

### [0.1.1](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/compare/v0.1.0...v0.1.1) (2020-05-15)


### Features

* add cloud run js backend template ([1bcac36](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/1bcac36e99ad6eb1f851f06d831294a17f158731))

## [0.1.0](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/compare/v0.0.3...v0.1.0) (2020-05-14)


### ⚠ BREAKING CHANGES

* add few items like version number

### Features

* improve based on uplb-hci-template ([1c626a9](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/1c626a9d06c5845f6b6fcba0d681e6d5fc9b795c))


### Bug Fixes

* merge conflicts ([a6a2ce9](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/a6a2ce9881e6a31304c13b6c4b7527b6309c7865))
* **deps:** update dependency chalk to v3 ([81134bf](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/81134bfd6f370fa9ad839cccc7f5d5026ca7a07f))
* **deps:** update dependency fs-extra to v9 ([f494de7](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/f494de7142e23d97c53d6f6c4d2857b4921ac11f))
* **deps:** update dependency yeoman-generator to v4 ([a38575d](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/a38575d7fb0cfdc5995b7459e54c13505751a760))

### [0.0.3](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/compare/v0.0.2...v0.0.3) (2020-03-04)


### Bug Fixes

* add fs-extra ([cbb5971](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/cbb597176952f6be92021e2bcd4b831b6573b432))

### 0.0.2 (2020-01-18)


### Features

* add ability to create base template ([7b8e4c7](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/7b8e4c7f14f36d9744e95dd86a8382f7575c10f2))
* add docs and add ability docs ([bbfb597](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/bbfb5973c2a6da9e3826154ae6808cb1fa289a34))
* add docs and add ability to add docs ([5badf09](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/5badf09bc05c4717b8b999381e3e818029e3d2ae))
* add python 3.7 template ([e0b22ad](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/e0b22ad2038504d24566b7f7f1b9b53cccc85205))
* add restana js template ([8b910a2](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/8b910a21dd98c2e4944ab5b715ef192a5d5bfa0c))
* first commit ([be3d956](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/be3d9564e1948edb112c0fc7a1d8ad579712ec12))


### Bug Fixes

* use senti-techlabs as scope ([eb43b1e](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/eb43b1e4082f6791d03ed4c9a839aea40b7516bf))
