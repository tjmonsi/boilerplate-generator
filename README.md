# Project Template Generator [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

This is the project generator for all tjmonsi created projects

# Installation

First, install [Yeoman](http://yeoman.io) and generator-project-template using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g @tjmonsi/generator-project-template
```

# Usage

Then generate your new project:

```bash
yo @tjmonsi/project-template
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

Apache-2.0 © [Toni-Jan Keith Monserrat]()


[npm-image]: https://badge.fury.io/js/generator-project-template-generator.svg
[npm-url]: https://npmjs.org/package/generator-project-template-generator
[travis-image]: https://travis-ci.com/tjmonsi/generator-project-template-generator.svg?branch=master
[travis-url]: https://travis-ci.com/tjmonsi/generator-project-template-generator
[daviddm-image]: https://david-dm.org/tjmonsi/generator-project-template-generator.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/tjmonsi/generator-project-template-generator
[coveralls-image]: https://coveralls.io/repos/tjmonsi/generator-project-template-generator/badge.svg
[coveralls-url]: https://coveralls.io/r/tjmonsi/generator-project-template-generator
